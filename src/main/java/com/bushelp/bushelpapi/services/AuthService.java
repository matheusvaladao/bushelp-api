package com.bushelp.bushelpapi.services;

import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.bushelp.bushelpapi.models.Usuario;
import com.bushelp.bushelpapi.repository.UsuarioRepository;

@Service
public class AuthService {

	private Random rand = new Random();

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public void sendNewPassword(String email) throws Exception {
		Optional<Usuario> usuarioOp = usuarioRepository.findByEmail(email);
		Usuario usuario = usuarioOp.get();
		if (usuario == null) {
			throw new Exception("Email não encontrado");
		}
		String newPass = newPassword();
		usuario.setSenha(bCryptPasswordEncoder.encode(newPass));
		usuarioRepository.save(usuario);
	}

	private String newPassword() {
		char[] vet = new char[10];
		for (int i = 0; i < 10; i++) {
			vet[i] = randomChar();
		}
		return new String(vet);
	}

	private char randomChar() {
		int opt = rand.nextInt(3);
		if (opt == 0) {// Gera Digito
			return (char) (rand.nextInt(10) + 48);
		} else if (opt == 1) {// Gera Maiuscula
			return (char) (rand.nextInt(26) + 65);
		} else {// Gera Minuscula
			return (char) (rand.nextInt(26) + 97);
		}
	}

}
