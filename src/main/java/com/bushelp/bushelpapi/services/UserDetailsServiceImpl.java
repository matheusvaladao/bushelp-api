package com.bushelp.bushelpapi.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.bushelp.bushelpapi.models.Usuario;
import com.bushelp.bushelpapi.repository.UsuarioRepository;
import com.bushelp.bushelpapi.security.UserSS;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

		Optional<Usuario> usuarioExistente = usuarioRepository.findByEmail(email);
		if (!usuarioExistente.isPresent()) {
			throw new UsernameNotFoundException(email);
		}
		Usuario usuario = usuarioExistente.get();
		return new UserSS(usuario.getId(), usuario.getEmail(), usuario.getSenha(), usuario.getUsuarioPerfis());
	}

}
