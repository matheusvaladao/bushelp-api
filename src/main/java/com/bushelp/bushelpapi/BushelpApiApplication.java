package com.bushelp.bushelpapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BushelpApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BushelpApiApplication.class, args);
	}

}
