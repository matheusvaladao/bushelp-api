package com.bushelp.bushelpapi.controllers;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bushelp.bushelpapi.models.Linha;
import com.bushelp.bushelpapi.models.Ponto;
import com.bushelp.bushelpapi.models.SequenciaPonto;
import com.bushelp.bushelpapi.repository.LinhaRepository;
import com.bushelp.bushelpapi.repository.PontoRepository;
import com.bushelp.bushelpapi.repository.SequenciaPontoRepository;

@RestController
@RequestMapping("/pontos")
public class PontoController {

	@Autowired
	PontoRepository pontoRepository;

	@Autowired
	LinhaRepository linhaRepository;

	@Autowired
	SequenciaPontoRepository sequenciaPontoRepository;

	@PostMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ResponseStatus(HttpStatus.CREATED)
	public Ponto adicionar(@Valid @RequestBody Ponto ponto) {
		return pontoRepository.save(ponto);
	}

	@PutMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	public Ponto alterar(@Valid @RequestBody Ponto ponto) {
		return pontoRepository.save(ponto);
	}

	@DeleteMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ResponseStatus(HttpStatus.OK)
	public Ponto excluir(@Valid @RequestBody Ponto ponto) {
		Optional<Ponto> optionalPonto = pontoRepository.findById(ponto.getId());
		Ponto pontoExcluido = optionalPonto.get();
		pontoRepository.deleteById(ponto.getId());
		return pontoExcluido;
	}

	@GetMapping
	public List<Ponto> listar() {
		return pontoRepository.findAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Ponto> buscarHorarioId(@PathVariable Long id) {
		Optional<Ponto> optionalPonto = pontoRepository.findById(id);
		return ResponseEntity.ok(optionalPonto.get());
	}

	@GetMapping("/linha/{id}")
	public List<Ponto> listarPontosLinha(@PathVariable Long id) {
		Optional<Linha> optionalLinha = linhaRepository.findById(id);
		Linha linha = optionalLinha.get();
		List<SequenciaPonto> sequenciaPontos = sequenciaPontoRepository.findAllByLinha(linha);
		List<Ponto> pontos = new LinkedList<Ponto>();
		for (SequenciaPonto sequenciaPonto : sequenciaPontos) {
			pontos.add(sequenciaPonto.getPonto());
		}
		return pontos;
	}

	@GetMapping("/rota/linha/{id}")
	public List<Ponto> listarPontosRotaLinha(@PathVariable Long id) {
		Optional<Linha> optionalLinha = linhaRepository.findById(id);
		Linha linha = optionalLinha.get();
		List<SequenciaPonto> sequenciaPontos = sequenciaPontoRepository.findAllByLinhaOrderByNumeroSequenciaAsc(linha);
		List<Ponto> pontos = new LinkedList<Ponto>();
		for (SequenciaPonto sequenciaPonto : sequenciaPontos) {
			pontos.add(sequenciaPonto.getPonto());
		}
		return pontos;
	}

	/**
	 * Metodo que calcula a distancia geografia entre dois pontos em linha reta.
	 * 
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @return
	 */
	private double distancia(String lat1S, String lon1S, String lat2S, String lon2S) {

		double lat1 = Double.parseDouble(lat1S);
		double lon1 = Double.parseDouble(lon1S);
		double lat2 = Double.parseDouble(lat2S);
		double lon2 = Double.parseDouble(lon2S);

		if ((lat1 == lat2) && (lon1 == lon2)) {
			return 0;
		} else {
			double theta = lon1 - lon2;
			double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2))
					+ Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
			dist = Math.acos(dist);
			dist = Math.toDegrees(dist);
			dist = dist * 60 * 1.1515;
			dist = dist * 1609.344;
			return (dist);
		}
	}

	@GetMapping("/linha/{numeroLinha}/latitude/{latitude}/longitude/{longitude}")
	public Ponto obterPontoPorLinhaECoordenada(@PathVariable String numeroLinha, @PathVariable String latitude,
			@PathVariable String longitude) {
		double distanciaMenor = 100000;
		Ponto ponto = null;
		Linha linha = linhaRepository.findByNumero(numeroLinha);
		List<SequenciaPonto> sequenciaPontos = sequenciaPontoRepository.findAllByLinhaOrderByNumeroSequenciaAsc(linha);

		for (SequenciaPonto sequenciaPonto : sequenciaPontos) {
			double dist = distancia(latitude, longitude, sequenciaPonto.getPonto().getLatitude(),
					sequenciaPonto.getPonto().getLongitude());
			if (dist < distanciaMenor) {
				distanciaMenor = dist;
				ponto = sequenciaPonto.getPonto();
			}
		}

		return ponto;
	}
}
