package com.bushelp.bushelpapi.controllers;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.bushelp.bushelpapi.models.Horario;
import com.bushelp.bushelpapi.models.Linha;
import com.bushelp.bushelpapi.models.Ponto;
import com.bushelp.bushelpapi.models.SequenciaPonto;
import com.bushelp.bushelpapi.repository.HorarioRepository;
import com.bushelp.bushelpapi.repository.LinhaRepository;

@RestController
@RequestMapping("/horarios")
public class HorarioControlller {

	@Autowired
	HorarioRepository horarioRepository;

	@Autowired
	LinhaRepository linhaRepository;

	@PostMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ResponseStatus(HttpStatus.CREATED)
	public Horario adicionar(@Valid @RequestBody Horario horario) {
		return horarioRepository.save(horario);
	}

	@PutMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	public Horario alterar(@Valid @RequestBody Horario horario) {
		return horarioRepository.save(horario);
	}

	@DeleteMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ResponseStatus(HttpStatus.OK)
	public Horario excluir(@Valid @RequestBody Horario horario) {
		Optional<Horario> optionalHorario = horarioRepository.findById(horario.getId());
		Horario horarioExcluido = optionalHorario.get();
		horarioRepository.deleteById(horario.getId());
		return horarioExcluido;
	}

	@GetMapping
	public List<Horario> listar() {
		return horarioRepository.findAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Horario> buscarHorarioId(@PathVariable Long id) {
		Optional<Horario> optionalHorario = horarioRepository.findById(id);
		return ResponseEntity.ok(optionalHorario.get());
	}

	@GetMapping("/linha/{id}")
	public List<Horario> listarHorariosLinha(@PathVariable Long id) {
		Optional<Linha> optionalLinha = linhaRepository.findById(id);
		Linha linha = optionalLinha.get();
		return horarioRepository.findAllByLinha(linha);
	}

}
