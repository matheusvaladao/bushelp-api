package com.bushelp.bushelpapi.controllers;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.bushelp.bushelpapi.models.UsuarioPerfil;
import com.bushelp.bushelpapi.models.Usuario;
import com.bushelp.bushelpapi.repository.UsuarioRepository;
import com.bushelp.bushelpapi.security.UserSS;
import com.bushelp.bushelpapi.services.UserService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	UsuarioRepository usuarioRepository;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Usuario adicionar(@Valid @RequestBody Usuario usuario) {
		Optional<Usuario> optionalUsuario = usuarioRepository.findByEmail(usuario.getEmail());
		if (optionalUsuario.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"Já existe um usuário cadastrado com esse e-mail.");
		}
		usuario.setSenha(passwordEncoder.encode(usuario.getSenha()));
		return usuarioRepository.save(usuario);
	}

	@PutMapping
	public Usuario alterar(@Valid @RequestBody Usuario usuario) {
		usuario.setSenha(passwordEncoder.encode(usuario.getSenha()));
		return usuarioRepository.save(usuario);
	}

	@DeleteMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ResponseStatus(HttpStatus.OK)
	public Usuario excluir(@Valid @RequestBody Usuario usuario) {
		Optional<Usuario> optionalUsuario = usuarioRepository.findById(usuario.getId());
		Usuario op = optionalUsuario.get();
		usuarioRepository.deleteById(usuario.getId());
		return op;
	}

	@GetMapping
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public List<Usuario> listar() {
		return usuarioRepository.findAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Usuario> buscarUsuarioId(@PathVariable Long id) {
		Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);

		UserSS user = UserService.authenticated();
		if (user == null || !user.hasRole(UsuarioPerfil.ADMIN) && !id.equals(user.getId())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Acesso Negado.");
		}

		if (!optionalUsuario.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(optionalUsuario.get());
	}

	@GetMapping("/email/{email}")
	public ResponseEntity<Usuario> buscarUsuarioEmail(@PathVariable String email) {
		Optional<Usuario> optionalUsuario = usuarioRepository.findByEmail(email);

		UserSS user = UserService.authenticated();
		if (user == null || !user.hasRole(UsuarioPerfil.ADMIN) && !email.equals(user.getUsername())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Acesso Negado.");
		}

		if (!optionalUsuario.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(optionalUsuario.get());
	}

}
