package com.bushelp.bushelpapi.controllers;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.bushelp.bushelpapi.models.SequenciaPonto;
import com.bushelp.bushelpapi.repository.SequenciaPontoRepository;

@RestController
@RequestMapping("/sequenciaPontos")
public class SequenciaPontoController {

	@Autowired
	SequenciaPontoRepository sequenciaPontoRepository;

	@PostMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ResponseStatus(HttpStatus.CREATED)
	public SequenciaPonto adicionar(@Valid @RequestBody SequenciaPonto sequenciaPonto) {
		return sequenciaPontoRepository.save(sequenciaPonto);
	}

	@PutMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	public SequenciaPonto alterar(@Valid @RequestBody SequenciaPonto sequenciaPonto) {
		return sequenciaPontoRepository.save(sequenciaPonto);
	}

	@DeleteMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ResponseStatus(HttpStatus.OK)
	public SequenciaPonto excluir(@Valid @RequestBody SequenciaPonto sequenciaPonto) {
		Optional<SequenciaPonto> optionalSequenciaPonto = sequenciaPontoRepository.findById(sequenciaPonto.getId());
		SequenciaPonto horarioExcluido = optionalSequenciaPonto.get();
		sequenciaPontoRepository.deleteById(sequenciaPonto.getId());
		return horarioExcluido;
	}

	@GetMapping
	public List<SequenciaPonto> listar() {
		return sequenciaPontoRepository.findAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<SequenciaPonto> buscarHorarioId(@PathVariable Long id) {
		Optional<SequenciaPonto> optionalSequenciaPonto = sequenciaPontoRepository.findById(id);
		return ResponseEntity.ok(optionalSequenciaPonto.get());
	}

}
