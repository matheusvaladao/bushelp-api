package com.bushelp.bushelpapi.controllers;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.bushelp.bushelpapi.models.Linha;
import com.bushelp.bushelpapi.models.Ponto;
import com.bushelp.bushelpapi.models.SequenciaPonto;
import com.bushelp.bushelpapi.repository.LinhaRepository;
import com.bushelp.bushelpapi.repository.PontoRepository;
import com.bushelp.bushelpapi.repository.SequenciaPontoRepository;

@RestController
@RequestMapping("/linhas")
public class LinhaController {

	@Autowired
	LinhaRepository linhaRepository;

	@Autowired
	PontoRepository pontoRepository;

	@Autowired
	SequenciaPontoRepository sequenciaPontoRepository;

	@PostMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ResponseStatus(HttpStatus.CREATED)
	public Linha adicionar(@Valid @RequestBody Linha linha) {
		return linhaRepository.save(linha);
	}

	@PutMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	public Linha alterar(@Valid @RequestBody Linha linha) {
		return linhaRepository.save(linha);
	}

	@DeleteMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ResponseStatus(HttpStatus.OK)
	public Linha excluir(@Valid @RequestBody Linha linha) {
		Optional<Linha> optionalLinha = linhaRepository.findById(linha.getId());
		Linha linhaExcluido = optionalLinha.get();
		linhaRepository.deleteById(linha.getId());
		return linhaExcluido;
	}

	@GetMapping
	public List<Linha> listar() {
		return linhaRepository.findAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Linha> buscarHorarioId(@PathVariable Long id) {
		Optional<Linha> optionalLinha = linhaRepository.findById(id);
		return ResponseEntity.ok(optionalLinha.get());
	}

	@GetMapping("/ponto/{id}")
	public List<Linha> listarLinhasPonto(@PathVariable Long id) {
		Optional<Ponto> optionalPonto = pontoRepository.findById(id);
		Ponto ponto = optionalPonto.get();
		List<SequenciaPonto> sequenciaPontos = sequenciaPontoRepository.findAllByPonto(ponto);
		List<Linha> linhas = new LinkedList<Linha>();
		for (SequenciaPonto sequenciaPonto : sequenciaPontos) {
			linhas.add(sequenciaPonto.getLinha());
		}
		return linhas;
	}

}
