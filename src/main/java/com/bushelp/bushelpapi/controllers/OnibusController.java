package com.bushelp.bushelpapi.controllers;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bushelp.bushelpapi.models.Linha;
import com.bushelp.bushelpapi.models.Onibus;
import com.bushelp.bushelpapi.models.Ponto;
import com.bushelp.bushelpapi.models.SequenciaPonto;
import com.bushelp.bushelpapi.repository.LinhaRepository;
import com.bushelp.bushelpapi.repository.OnibusRepository;
import com.bushelp.bushelpapi.repository.PontoRepository;
import com.bushelp.bushelpapi.repository.SequenciaPontoRepository;

import io.jsonwebtoken.lang.Collections;

@RestController
@RequestMapping("/onibus")
public class OnibusController {

	@Autowired
	OnibusRepository onibusRepository;

	@Autowired
	LinhaRepository linhaRepository;

	@Autowired
	PontoRepository pontoRepository;

	@Autowired
	SequenciaPontoRepository sequenciaPontoRepository;

	@PostMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ResponseStatus(HttpStatus.CREATED)
	public Onibus adicionar(@Valid @RequestBody Onibus onibus) {
		return onibusRepository.save(onibus);
	}

	@PutMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	public Onibus alterar(@Valid @RequestBody Onibus onibus) {
		return onibusRepository.save(onibus);
	}

	@DeleteMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ResponseStatus(HttpStatus.OK)
	public Onibus excluir(@Valid @RequestBody Onibus onibus) {
		Optional<Onibus> optionalOnibus = onibusRepository.findById(onibus.getId());
		Onibus onibusExcluido = optionalOnibus.get();
		onibusRepository.deleteById(onibus.getId());
		return onibusExcluido;
	}

	@GetMapping
	public List<Onibus> listar() {
		return onibusRepository.findAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Onibus> buscarHorarioId(@PathVariable Long id) {
		Optional<Onibus> optionalOnibus = onibusRepository.findById(id);
		return ResponseEntity.ok(optionalOnibus.get());
	}

	@GetMapping("/ponto/{idPonto}/linha/{idLinha}")
	public ResponseEntity<Onibus> buscarProximoOnibus(@PathVariable Long idPonto, @PathVariable Long idLinha) {
		Optional<Linha> optionalLinha = linhaRepository.findById(idLinha);
		Linha linha = optionalLinha.get();
		List<SequenciaPonto> sequenciaPontos = sequenciaPontoRepository.findAllByLinhaOrderByNumeroSequenciaAsc(linha);
		List<Ponto> pontos = new LinkedList<Ponto>();

		for (SequenciaPonto sequenciaPonto : sequenciaPontos) {
			pontos.add(sequenciaPonto.getPonto());
			if (sequenciaPonto.getPonto().getId() == idPonto) {
				break;
			}
		}
		java.util.Collections.reverse(pontos);

		Optional<Onibus> optionalOnibus = Optional.ofNullable(null);

		for (Ponto ponto : pontos) {
			List<Onibus> proximosOnibus = onibusRepository.findAllByProximoPonto(ponto);
			for (Onibus onibus : proximosOnibus) {
				if (onibus.getLinha().equals(linha)) {
					optionalOnibus = Optional.of(onibus);
					return ResponseEntity.ok(optionalOnibus.get());
				}
			}
		}

		return ResponseEntity.ok(optionalOnibus.get());
	}

	/**
	 * Metodo que calcula a distancia geografia entre dois pontos em linha reta.
	 * 
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @return
	 */
	private double distancia(String lat1S, String lon1S, String lat2S, String lon2S) {

		double lat1 = Double.parseDouble(lat1S);
		double lon1 = Double.parseDouble(lon1S);
		double lat2 = Double.parseDouble(lat2S);
		double lon2 = Double.parseDouble(lon2S);

		if ((lat1 == lat2) && (lon1 == lon2)) {
			return 0;
		} else {
			double theta = lon1 - lon2;
			double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2))
					+ Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
			dist = Math.acos(dist);
			dist = Math.toDegrees(dist);
			dist = dist * 60 * 1.1515;
			dist = dist * 1609.344;
			return (dist);
		}
	}

	@GetMapping("/linha/{numeroLinha}/latitude/{latitude}/longitude/{longitude}")
	public ResponseEntity<Onibus> obterOnibusPorLinhaECoordenada(@PathVariable String numeroLinha,
			@PathVariable String latitude, @PathVariable String longitude) {

		double distanciaMenor = 100000;
		Ponto ponto = null;
		Onibus onibus = null;

		Linha linha = linhaRepository.findByNumero(numeroLinha);
		List<SequenciaPonto> sequenciaPontos = sequenciaPontoRepository.findAllByLinhaOrderByNumeroSequenciaAsc(linha);

		for (SequenciaPonto sequenciaPonto : sequenciaPontos) {
			double dist = distancia(latitude, longitude, sequenciaPonto.getPonto().getLatitude(),
					sequenciaPonto.getPonto().getLongitude());
			if (dist < distanciaMenor) {
				distanciaMenor = dist;
				ponto = sequenciaPonto.getPonto();
			}
		}

		/**
		 * Ponto encontrado. Encontrar onibus.
		 */

		List<Ponto> pontos = new LinkedList<Ponto>();
		for (SequenciaPonto sequenciaPonto : sequenciaPontos) {
			pontos.add(sequenciaPonto.getPonto());
			if (sequenciaPonto.getPonto().getId() == ponto.getId()) {
				break;
			}
		}
		java.util.Collections.reverse(pontos);

		Optional<Onibus> optionalOnibus = Optional.ofNullable(null);

		for (Ponto p : pontos) {
			List<Onibus> proximosOnibus = onibusRepository.findAllByProximoPonto(p);
			for (Onibus o : proximosOnibus) {
				if (o.getLinha().equals(linha)) {
					optionalOnibus = Optional.of(o);
					return ResponseEntity.ok(optionalOnibus.get());
				}
			}
		}

		return ResponseEntity.ok(optionalOnibus.get());
	}

}
