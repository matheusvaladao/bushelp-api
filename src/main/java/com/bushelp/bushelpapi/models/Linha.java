package com.bushelp.bushelpapi.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.UniqueElements;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "linhas")
public class Linha {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotEmpty
	@UniqueElements
	private String numero;
	@NotEmpty
	private String nome;
	@JsonIgnore
	@OneToMany(mappedBy = "linha")
	private List<Horario> horarios;
	@JsonIgnore
	@OneToMany(mappedBy = "linha")
	private List<SequenciaPonto> sequenciaPontos;
	@JsonIgnore
	@OneToMany(mappedBy = "linha")
	private List<Onibus> onibus;

	public Linha() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Linha(long id, @NotEmpty String numero, @NotEmpty String nome, List<Horario> horarios,
			List<SequenciaPonto> sequenciaPontos, List<Onibus> onibus) {
		super();
		this.id = id;
		this.numero = numero;
		this.nome = nome;
		this.horarios = horarios;
		this.sequenciaPontos = sequenciaPontos;
		this.onibus = onibus;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Horario> getHorarios() {
		return horarios;
	}

	public void setHorarios(List<Horario> horarios) {
		this.horarios = horarios;
	}

	public List<SequenciaPonto> getSequenciaPontos() {
		return sequenciaPontos;
	}

	public void setSequenciaPontos(List<SequenciaPonto> sequenciaPontos) {
		this.sequenciaPontos = sequenciaPontos;
	}

	public List<Onibus> getOnibus() {
		return onibus;
	}

	public void setOnibus(List<Onibus> onibus) {
		this.onibus = onibus;
	}

}
