package com.bushelp.bushelpapi.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "pontos")
public class Ponto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotEmpty
	private String descricao;
	@NotEmpty
	private String numero;
	@JsonIgnore
	@OneToMany(mappedBy = "ponto")
	private List<SequenciaPonto> sequenciaPontos;
	@NotEmpty
	private String latitude;
	@NotEmpty
	private String longitude;
	@JsonIgnore
	@OneToMany(mappedBy = "proximoPonto")
	private List<Onibus> onibusVindo;

	public Ponto() {
		super();
	}

	public Ponto(long id, @NotEmpty String descricao, @NotEmpty String numero, List<SequenciaPonto> sequenciaPontos,
			@NotEmpty String latitude, @NotEmpty String longitude, List<Onibus> onibusVindo) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.numero = numero;
		this.sequenciaPontos = sequenciaPontos;
		this.latitude = latitude;
		this.longitude = longitude;
		this.onibusVindo = onibusVindo;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public List<SequenciaPonto> getSequenciaPontos() {
		return sequenciaPontos;
	}

	public void setSequenciaPontos(List<SequenciaPonto> sequenciaPontos) {
		this.sequenciaPontos = sequenciaPontos;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public List<Onibus> getOnibusVindo() {
		return onibusVindo;
	}

	public void setOnibusVindo(List<Onibus> onibusVindo) {
		this.onibusVindo = onibusVindo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((onibusVindo == null) ? 0 : onibusVindo.hashCode());
		result = prime * result + ((sequenciaPontos == null) ? 0 : sequenciaPontos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ponto other = (Ponto) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id != other.id)
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (onibusVindo == null) {
			if (other.onibusVindo != null)
				return false;
		} else if (!onibusVindo.equals(other.onibusVindo))
			return false;
		if (sequenciaPontos == null) {
			if (other.sequenciaPontos != null)
				return false;
		} else if (!sequenciaPontos.equals(other.sequenciaPontos))
			return false;
		return true;
	}

}
