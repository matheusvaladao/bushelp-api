package com.bushelp.bushelpapi.models;

public enum UsuarioPerfil {

	ADMIN(1, "ROLE_ADMIN"), 
	USUARIO(2, "ROLE_CLIENTE");

	private int codigo;
	private String descricao;

	private UsuarioPerfil(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public static UsuarioPerfil toEnum(Integer cod) {
		if (cod == null) {
			return null;
		}
		for (UsuarioPerfil perfil : UsuarioPerfil.values()) {
			if (cod.equals(perfil.getCodigo())) {
				return perfil;
			}
		}
		throw new IllegalArgumentException("Id inválido: " + cod);
	}

}
