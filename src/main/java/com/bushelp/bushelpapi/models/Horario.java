package com.bushelp.bushelpapi.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "horarios")
public class Horario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull
	@Enumerated
	private HorarioTipo tipo;

	@NotEmpty
	private String turno;

	@NotEmpty
	private String intervalo;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "linha_id")
	private Linha linha;

	public Horario() {
		super();
	}

	public Horario(long id, HorarioTipo tipo, @NotEmpty String turno, @NotEmpty String intervalo, Linha linha) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.turno = turno;
		this.intervalo = intervalo;
		this.linha = linha;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public HorarioTipo getTipo() {
		return tipo;
	}

	public void setTipo(HorarioTipo tipo) {
		this.tipo = tipo;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public String getIntervalo() {
		return intervalo;
	}

	public void setIntervalo(String intervalo) {
		this.intervalo = intervalo;
	}

	public Linha getLinha() {
		return linha;
	}

	public void setLinha(Linha linha) {
		this.linha = linha;
	}

}
