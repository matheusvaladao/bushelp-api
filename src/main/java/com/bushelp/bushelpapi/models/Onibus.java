package com.bushelp.bushelpapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Onibus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotEmpty
	private String numero;
	@NotEmpty
	private String placa;
	@Column(name = "id_beacon")
	private String idBeacon;
	@NotNull
	private boolean servico;
	private String latitude;
	private String longitude;
	@ManyToOne
	@JoinColumn(name = "linha_id")
	private Linha linha;
	@ManyToOne
	@JoinColumn(name = "proximo_ponto")
	private Ponto proximoPonto;

	public Onibus() {
		super();
	}

	public Onibus(long id, @NotEmpty String numero, @NotEmpty String placa, String idBeacon, @NotNull boolean servico,
			String latitude, String longitude, Linha linha, Ponto proximoPonto) {
		super();
		this.id = id;
		this.numero = numero;
		this.placa = placa;
		this.idBeacon = idBeacon;
		this.servico = servico;
		this.latitude = latitude;
		this.longitude = longitude;
		this.linha = linha;
		this.proximoPonto = proximoPonto;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getIdBeacon() {
		return idBeacon;
	}

	public void setIdBeacon(String idBeacon) {
		this.idBeacon = idBeacon;
	}

	public boolean isServico() {
		return servico;
	}

	public void setServico(boolean servico) {
		this.servico = servico;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Linha getLinha() {
		return linha;
	}

	public void setLinha(Linha linha) {
		this.linha = linha;
	}

	public Ponto getProximoPonto() {
		return proximoPonto;
	}

	public void setProximoPonto(Ponto proximoPonto) {
		this.proximoPonto = proximoPonto;
	}

}
