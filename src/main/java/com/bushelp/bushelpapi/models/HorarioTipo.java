package com.bushelp.bushelpapi.models;

public enum HorarioTipo {

	DIA_UTIL(0), SABADO(1), DOMINGO_FERIADO(2);

	private int codigoTipo;

	private HorarioTipo(int codigoTipo) {
		this.codigoTipo = codigoTipo;
	}

	public int getCodigoTipo() {
		return codigoTipo;
	}

	public void setCodigoTipo(int codigoTipo) {
		this.codigoTipo = codigoTipo;
	}

	public static HorarioTipo toEnum(Integer codigoTipo) {
		if (codigoTipo == null) {
			return null;
		}
		for (HorarioTipo tipo : HorarioTipo.values()) {
			if (codigoTipo.equals(tipo.getCodigoTipo())) {
				return tipo;
			}
		}
		throw new IllegalArgumentException("Tipo de horário inválido: " + codigoTipo);
	}

}
