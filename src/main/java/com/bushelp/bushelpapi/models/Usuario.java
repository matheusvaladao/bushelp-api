package com.bushelp.bushelpapi.models;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "usuarios")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotEmpty
	private String nome;

	@NotEmpty
	private String sobrenome;

	@NotEmpty
	private String telefone;

	@NotEmpty
	@NotNull
	@Column(unique = true)
	private String email;

	@NotEmpty
	private String senha;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "usuarios_perfis")
	private Set<Integer> usuarioPerfis = new HashSet<>();

	public Usuario() {
		super();
		addUsuarioPerfil(UsuarioPerfil.USUARIO);
	}

	public Usuario(long id, @NotEmpty String nome, @NotEmpty String sobrenome, @NotEmpty String telefone,
			@NotEmpty @NotNull String email, @NotEmpty String senha, Set<Integer> usuarioPerfis) {
		super();
		this.id = id;
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.telefone = telefone;
		this.email = email;
		this.senha = senha;
		this.usuarioPerfis = usuarioPerfis;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setUsuarioPerfis(Set<Integer> usuarioPerfis) {
		this.usuarioPerfis = usuarioPerfis;
	}

	public Set<UsuarioPerfil> getUsuarioPerfis() {
		return usuarioPerfis.stream().map(x -> UsuarioPerfil.toEnum(x)).collect(Collectors.toSet());
	}

	public void addUsuarioPerfil(UsuarioPerfil usuarioPerfil) {
		usuarioPerfis.add(usuarioPerfil.getCodigo());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		result = prime * result + ((sobrenome == null) ? 0 : sobrenome.hashCode());
		result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
		result = prime * result + ((usuarioPerfis == null) ? 0 : usuarioPerfis.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		if (sobrenome == null) {
			if (other.sobrenome != null)
				return false;
		} else if (!sobrenome.equals(other.sobrenome))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		if (usuarioPerfis == null) {
			if (other.usuarioPerfis != null)
				return false;
		} else if (!usuarioPerfis.equals(other.usuarioPerfis))
			return false;
		return true;
	}

}
