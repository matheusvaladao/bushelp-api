package com.bushelp.bushelpapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "sequencia_pontos")
public class SequenciaPonto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull
	@Column(name = "numero_sequencia")
	private int numeroSequencia;
	@NotNull
	@ManyToOne
	@JoinColumn(name = "linha_id")
	private Linha linha;
	@NotNull
	@ManyToOne
	@JoinColumn(name = "ponto_id")
	private Ponto ponto;

	public SequenciaPonto() {
		super();
	}

	public SequenciaPonto(long id, int numeroSequencia, Linha linha, Ponto ponto) {
		super();
		this.id = id;
		this.numeroSequencia = numeroSequencia;
		this.linha = linha;
		this.ponto = ponto;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNumeroSequencia() {
		return numeroSequencia;
	}

	public void setNumeroSequencia(int numeroSequencia) {
		this.numeroSequencia = numeroSequencia;
	}

	public Linha getLinha() {
		return linha;
	}

	public void setLinha(Linha linha) {
		this.linha = linha;
	}

	public Ponto getPonto() {
		return ponto;
	}

	public void setPonto(Ponto ponto) {
		this.ponto = ponto;
	}

}
