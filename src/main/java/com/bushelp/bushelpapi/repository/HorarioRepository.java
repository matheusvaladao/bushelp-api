package com.bushelp.bushelpapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bushelp.bushelpapi.models.Horario;
import com.bushelp.bushelpapi.models.Linha;

public interface HorarioRepository extends JpaRepository<Horario, Long> {

	List<Horario> findAllByLinha(Linha linha);

}
