package com.bushelp.bushelpapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bushelp.bushelpapi.models.Linha;
import com.bushelp.bushelpapi.models.Ponto;
import com.bushelp.bushelpapi.models.SequenciaPonto;

public interface SequenciaPontoRepository extends JpaRepository<SequenciaPonto, Long> {

	List<SequenciaPonto> findAllByLinha(Linha linha);

	List<SequenciaPonto> findAllByPonto(Ponto ponto);

	List<SequenciaPonto> findAllByLinhaOrderByNumeroSequenciaAsc(Linha linha);

}
