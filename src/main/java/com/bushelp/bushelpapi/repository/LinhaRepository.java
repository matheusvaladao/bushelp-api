package com.bushelp.bushelpapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bushelp.bushelpapi.models.Linha;

public interface LinhaRepository extends JpaRepository<Linha, Long> {

	Linha findByNumero(String numero);

}
