package com.bushelp.bushelpapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bushelp.bushelpapi.models.Linha;
import com.bushelp.bushelpapi.models.Ponto;

public interface PontoRepository extends JpaRepository<Ponto, Long> {

}
