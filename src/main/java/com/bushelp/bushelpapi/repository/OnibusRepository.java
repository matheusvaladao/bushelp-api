package com.bushelp.bushelpapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bushelp.bushelpapi.models.Onibus;
import com.bushelp.bushelpapi.models.Ponto;

public interface OnibusRepository extends JpaRepository<Onibus, Long> {

	List<Onibus> findAllByProximoPonto(Ponto ponto);
}
