create table usuario (

	id bigint auto_increment not null,
	nome varchar(80) not null,
	sobrenome varchar(80) not null,
	email varchar(80) unique not null,
	senha varchar(100) not null,
	
	primary key (id)
);